// JavaScript Document
window.onload = function() {
  const spinner = document.getElementById('loading');
 
  // Add .loaded to .loading
  spinner.classList.add('loaded');
}

$(function (){
	//
	var navPos = jQuery( '.content' ).offset().top; // グローバルメニューの位置
	var navHeight = jQuery( '.header' ).outerHeight(); // グローバルメニューの高さ
		jQuery( window ).on( 'scroll', function() {
			if ( jQuery( this ).scrollTop() > navPos ) {
				jQuery( 'body' ).css( 'padding-top', 0 );
				jQuery( '.header' ).addClass( 'm_fixed' );
				jQuery( '.menu-btn' ).addClass( 'm_fixed' );
				jQuery( '.glovalNav  .sns--insta' ).addClass( 'm_fixed' );
		} else {
			jQuery( 'body' ).css( 'padding-top', 0 );
			jQuery( '.header' ).removeClass( 'm_fixed' );
			jQuery( '.menu-btn' ).removeClass( 'm_fixed' );
			jQuery( '.glovalNav  .sns--insta' ).removeClass( 'm_fixed' );
		}
	});
	
	// fade in
	$(window).scroll(function (){
        $('.fadein').each(function(){
            var targetElement = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > targetElement - windowHeight + 80){
                $(this).css('opacity','1');
                $(this).css('transform','translateY(0)');
            }
        });
    });
	
	// footer list
	$(window).scroll(function (){
        $('.fadein--02').each(function(){
            var targetElement = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > targetElement - windowHeight + 30){
                $(this).css('opacity','1');
                $(this).css('transform','translateY(0)');
            }
        });
    });
	
	//
	$(window).on("load", function() {
		$('.kv--area .for-sp.kv--image').delay(0).css('background-size','auto 130vh');
	});
	
	//開閉用ボタンをクリックでクラスの切替え
	var $body = $('body');
	//開閉用ボタンをクリックでクラスの切替え
	$('.js__btn').on('click', function () {
		$body.toggleClass('open');
	});
    $('.global-nav').on('click', function () {
        $body.removeClass('open');
    });
	

	
	//メニュー名以外の部分をクリックで閉じる
//    $('#sp-global-nav ul li a').on('click', function () {
//        $body.removeClass('open');
//    });
	$('.toggle_contents li a').click(function(){
		$body.removeClass('open');
		$('.toggle_switch').next('ul').slideUp();
	});
	
	//.toggle_switch要素がクリックされたら
	$('.toggle_switch').click(function(){
		//クリックされた.accordion2の中のp要素に隣接する.accordion2の中の.innerを開いたり閉じたりする。
		$(this).next('ul').slideToggle();		
		//クリックされた.accordion2の中のp要素以外の.accordion2の中のp要素に隣接する.accordion2の中の.innerを閉じる
		$('.toggle_switch').not($(this)).next('ul').slideUp();
	});
	
});

/*==================================================
    scroll
==================================================*/
if (window.matchMedia( 'screen and (min-width: 768px)' ).matches) {
	// for pc
} else {
	// for sp

};

$(window).on('load', function() {
	var headerHeight = 0;
	var url = $(location).attr('href');
	if(url.indexOf("?id=") != -1){
		var id = url.split("?id=");
		var $target = $('#' + id[id.length - 1]);
		if($target.length){
			var pos = $target.offset().top-headerHeight;
			$("html, body").animate({scrollTop:pos}, 400);
		}
	}
});