$(function () {
    $(window).scroll(function () {
        var value = $(this).scrollTop(); //スクロールの値を取得
        var glovalNav1 = $('.kv--area .menu-btn');
        var glovalNav2 = $('.kv--area .sns--insta');
        var KvText = $('.kv--txt');
        if (value > 5) {
            glovalNav1.fadeOut(700);
            glovalNav2.fadeOut(700);
            KvText.fadeOut(700);
        } else {
            glovalNav1.fadeIn(700);
            glovalNav2.fadeIn(700);
            KvText.fadeIn(700);
        }

    });
});